#include <RF24.h>
byte address[6] = {'V', 'E', 'L', 'A', 'B'};
const int PACKETSIZE = 8;
byte packet[PACKETSIZE]; //holds the data
int radioChannel = 40; //change this
RF24 radio(9, 7); // CE, CSN
long t;

void setup() {
  // put your setup code here, to run once:
  radio.begin();
  radio.setChannel(radioChannel);
  radio.setPALevel(RF24_PA_MAX);
  radio.stopListening();
  radio.openWritingPipe(address);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  //analogReadResolution(12);
//  Serial.print("A0: ");
//  Serial.println( analogRead(A0));
//  Serial.print("A1: ");
//  Serial.println( analogRead(A1));
//  Serial.print("A2: ");
//  Serial.println( analogRead(A2));
  if (millis() - t > 13) {
    analogReadResolution(12);
    long i = analogRead(A0);
    t = millis();
    long *longPacket = (long *) packet;
    longPacket[0] = i;
    Serial.println(i);
    radio.write((void*)packet, PACKETSIZE, false);
  }
}
