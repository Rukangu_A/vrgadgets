#include <HX711_ADC.h>
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
//HX711 constructor (dout pin, sck pin):
HX711_ADC LoadCell(2, 3);
//HX711_ADC LoadCell(4, 5);
//const int eepromAdress = 0;

long t;

const int PACKETSIZE = 8;
byte packet[PACKETSIZE]; //holds the data
int radioChannel = 40; //change this
//byte address[6] = {"12345"}; //change this
byte address[6] = {'V', 'E', 'L', 'A', 'B'};
// RF24 radio(8, 9);  // CE, CSN
RF24 radio(10, 9);  // CE, CSN //rfnano
void setup() {

  radio.begin();
  radio.printDetails();
  radio.setChannel(radioChannel);
  radio.setPALevel(RF24_PA_HIGH);
  radio.stopListening();
  //radio.printDetails();
  radio.openWritingPipe(address);

  Serial.begin(115200);
  LoadCell.begin();
  //long stabilisingtime = 2000; // tare preciscion can be improved by adding a few seconds of stabilising time
  //.start(stabilisingtime);
}

void loop() {
  Serial.println("test");
  //update() should be called at least as often as HX711 sample rate; >10Hz@10SPS, >80Hz@80SPS
  //use of delay in sketch will reduce effective sample rate (be carefull with use of delay() in the loop)
  if (millis() - t > 13) {
    //long i = LoadCell.getSingleConversionVel();
    long i = 80808;
    t = millis();
    long *longPacket = (long *) packet;
    //longPacket[0] = t;
    longPacket[0] = i;
    Serial.println(i);
    if (i < 4000000 && i > -1000000) {
      //Serial.println(longPacket[0]);
      Serial.println("Sending");
      radio.write((void*)packet, PACKETSIZE, false);
    }
  }
}
